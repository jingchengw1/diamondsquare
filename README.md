-----COMP30019 Assignment 1 
-----JingCheng Wang 691808 jingchengw1
-----Chenxin Shen 729696

 chenxins Pull and open the project with Unity,double click "terrain" scence and click play to run the game.
------------------Terrain
We create a mesh to generate the terrain. 
Diamond-square algorithm is used to create hightmap for the mesh, to determine the "height" for every vertices.
The color of every vertices is determined by its height(the value of y axis). This is implemented in CreateMesh script.
------------------Water section
The script CreateWater is to render a part of the terrain mesh to be semi-transparent water. We attached a material to this section and used legacy-transparent-diffuse shader.
water section is placed in the center of terrain,and a "hole" is also intentionally made so that the water section looks realistic.
------------------Camera motion
We write a script CameraControl to control the camera motion.It has mouse controll as well as WASD QE controll.
Note that the mounse controll needs a combination of click left mounse button and drag.(We decide to do this because it allows a more precise controll)

A meshcollider is added to the terrain mesh, and a rigidbody component and a box collider are added to the camera, so that the camera collides with the terrain. 

To confine the camera within the terrain,we built 4 invisible walls with box collider to block the camera.
------------------Lighting
There is a "sun" in the scene which rotates around x axis to simulate sunrise and sunset. 
"PhongShader" is used to present Phong illumination model based lighting. 

External source:
-Our diamond-square algorithm is adapted from the website:http://stackoverflow.com/questions/2755750/diamond-square-algorithm
with adjusted parameters.
-Camera controll script retrieved some ideas from http://coffeebreakcodes.com/move-zoom-and-rotate-camera-unity3d/
-The phong shader is adapted from the lab5 solution written by Alex, with adjusted parameters.
-Unity's build-in shader legacy-transparent-diffuse shader is used for the water section

Important notes:
Depend on the version of Unity,it might only accept either "_Object2World" or "unity_ObjectToWorld" expression.
If either one is causing the Phongshader to report error,try to change to another one.

The terrain takes about 1 minute to generate(for the Surface Pro 3).