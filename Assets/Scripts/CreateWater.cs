﻿using UnityEngine;
using System.Collections;

//This class uses similar logic as the CreateMesh class,the purpose of the class
//is to create a water section in the center of the terrain.
public class CreateWater : MonoBehaviour {

    public int heightReso;
    public Sun sun;
    public Material material;
    void Start()
    {
        MeshFilter waterMesh = this.gameObject.AddComponent<MeshFilter>();
        waterMesh.mesh = this.CreateWaterMesh();

        MeshRenderer renderer = this.gameObject.AddComponent<MeshRenderer>();
        renderer.material=material;
        

        MeshCollider collider = this.gameObject.AddComponent<MeshCollider>();
        collider.sharedMesh = waterMesh.mesh;
        collider.convex = true;

    }

   

    Mesh CreateWaterMesh()
    {
        Mesh m = new Mesh();
        m.name = "Water";

        Vector3[] vertices = ConstructAllGrids().ToArray(typeof(Vector3)) as Vector3[];
        m.vertices = vertices;

        Color[] colors = new Color[vertices.Length];
        for(int i=0;i<vertices.Length;i++)
        {
            colors[i] = Color.blue;
        }
        m.colors = colors;
        //construct all triangles
        int[] triangles = new int[m.vertices.Length];
        for (int i = 0; i < m.vertices.Length; i++)
            triangles[i] = i;

        m.triangles = triangles;

        


        return m;
    }

    public ArrayList ConstructAllGrids()
    {
        ArrayList grids = new ArrayList();
        for (int i = (int)(heightReso * 0.4); i < heightReso * 0.7; i++)
        {
            for (int j = (int)(heightReso * 0.4); j < (int)(heightReso * 0.7); j++)
            {
                grids.AddRange(ConstructOneGrid(new Vector2(i, j)));
            }
        }

        return grids;
    }

    public ArrayList ConstructOneGrid(Vector2 coor)
    {
        float height = 15;
        ArrayList grid = new ArrayList();
        //bottom triangle
            grid.Add(new Vector3(coor.x + 1, height, coor.y + 1));
            grid.Add(new Vector3(coor.x + 1, height, coor.y));
            grid.Add(new Vector3(coor.x, height, coor.y));


        //top triangle
            grid.Add(new Vector3(coor.x, height, coor.y + 1));
            grid.Add(new Vector3(coor.x + 1, height, coor.y + 1));
            grid.Add(new Vector3(coor.x, height, coor.y));



        return grid;

    }
}
