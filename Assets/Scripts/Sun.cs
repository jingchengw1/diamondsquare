﻿using UnityEngine;
using System.Collections;

public class Sun : MonoBehaviour {


    public Color color;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
        //rotate the sun
		transform.RotateAround (Vector3.zero, Vector3.right, 10f * Time.deltaTime);
	}

    
}
