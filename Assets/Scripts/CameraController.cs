﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {


    public float cameraMoveSpeed;
    private float rotationX;
    private float rotationY;


    //sensitivity parameters for the camera controll
    private float sensitivityX = 100F;
    private float sensitivityY = 100F;
    private float minimumX = -360F;
    private float maximumX = 360F;
    private float minimumY = -60F;
    private float maximumY = 60F;


    // Use this for initialization
    void Start () {
        rotationX = transform.localEulerAngles.y;//still need to fix
        rotationY = -transform.localEulerAngles.x;


    }

    // Update is called once per frame
    void LateUpdate () {
        wasdControll();
        mouseControll();
        qeControll();
    }

    //controll for WASD
    public void wasdControll()
    {
        float translateX = 0;
        float translateY = 0;

        if (Input.GetKey("a"))
        {
            translateY += cameraMoveSpeed;
        }
        else if (Input.GetKey("d"))
        {
            translateY -= cameraMoveSpeed;
        }
        else if (Input.GetKey("w"))
        {
            translateX += cameraMoveSpeed;
        }
        else if (Input.GetKey("s"))
        {
            translateX -= cameraMoveSpeed;
        }
        Vector3 translation = new Vector3(-translateY, 0, translateX);
        this.transform.Translate(translation);
    }

    //controll for mouse
    public void mouseControll()
    {

        if (Input.GetMouseButton(0))//0 is left mouse,1 is the right.
        {
            rotationX += Input.GetAxis("Mouse X") * sensitivityX * Time.deltaTime;
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY * Time.deltaTime;
            rotationY = Mathf.Clamp(rotationY, -65f, 65f);
            transform.localEulerAngles = new Vector3(-rotationY, rotationX, transform.localEulerAngles.z);
            //change the x and y rotation but not the z
        }
    }

    //QE controll the roll
    public void qeControll()
    {
        if (Input.GetKey("q"))
        {
            transform.Rotate(Vector3.forward * sensitivityX * Time.deltaTime);
        }
        else if (Input.GetKey("e"))
        {
            transform.Rotate(Vector3.back * sensitivityX * Time.deltaTime);

        }
    }
    
    
}
