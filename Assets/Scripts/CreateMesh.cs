﻿using UnityEngine;
using System.Collections;

public class CreateMesh : MonoBehaviour
{
    public int heightReso;
    private float[,] heightMap;

    public Shader shader;
    public Sun sun;

    void Start()
    {
        MeshFilter terrainMesh = this.gameObject.AddComponent<MeshFilter>();
        terrainMesh.mesh = this.CreateTerrainMesh();

        MeshRenderer renderer = this.gameObject.AddComponent<MeshRenderer>();
        renderer.material.shader = shader;

        //add the collider so that the terrain collides with the camera
        MeshCollider collider = this.gameObject.AddComponent<MeshCollider>();
        collider.sharedMesh = terrainMesh.mesh;
        collider.convex = true;

    }

    void Update()
    {
        //update the look of the terrain each frame according to the shader,and the pos of the sun
        MeshRenderer renderer = this.gameObject.GetComponent<MeshRenderer>();
        renderer.material.SetColor("_PointLightColor", sun.color);
        
        renderer.material.SetVector("_PointLightPosition", sun.transform.position);
    }

    Mesh CreateTerrainMesh()
    {
        Mesh m = new Mesh();
        m.name = "Terrain";
        //generate height map
        heightMap = DiamondSquareAlgorithm();
        //make hole for water section
        MakeHole();
        //define the vertices
        Vector3[] vertices = ConstructAllGrids(heightReso - 3).ToArray(typeof(Vector3)) as Vector3[];
        m.vertices = vertices;
        //define the colors
        m.colors = GenerateColors(vertices);
        //construct all triangles
        int[] triangles = new int[m.vertices.Length];
        for (int i = 0; i < m.vertices.Length; i++)
            triangles[i] = i;

        m.triangles = triangles;
        //set normal
        Vector3[] normal = new Vector3[m.vertices.Length];
        for (int i = 0; i < m.vertices.Length; i++)
        {
            normal[i] = m.vertices[i].normalized;
        }
        m.normals = normal;

        return m;
    }


    //DiamondSquare algorithm to generate a 2D heightMap
    float[,] DiamondSquareAlgorithm()
    {
        float[,] heightMap = new float[heightReso, heightReso];

        int seed = Random.Range(1, 100);
        float range = seed / 2;

        //set the value for the 4 corners(randomly)
        heightMap[0, 0] = seed;
        heightMap[0, heightReso - 1] = seed;
        heightMap[heightReso - 1, 0] = seed;
        heightMap[heightReso - 1, heightReso - 1] = seed;

        //perform the calculation
        for (int cornerPoint = heightReso - 1; cornerPoint >= 2; cornerPoint /= 2, range /= 2)
        {

            //diamond step
            int centerPoint = cornerPoint / 2;
            for (int i = 0; i < heightReso - 1; i += cornerPoint)
            {
                for (int j = 0; j < heightReso - 1; j += cornerPoint)
                {
                    float avg = (heightMap[i, j] + heightMap[i, j + cornerPoint] +
                        heightMap[i + cornerPoint, j] + heightMap[i + cornerPoint, j + cornerPoint]) / 4;

                    float offset = Random.Range(0.0f, 1.0f) * 2 * range - range;//random a offset thats within range
                    heightMap[i + centerPoint, j + centerPoint] = avg + offset;
                }
            }
            //square step
            for (int i = 0; i < heightReso - 1; i += centerPoint)
            {
                for (int j = (i + centerPoint) % cornerPoint; j < heightReso - 1; j += cornerPoint)
                {

                    float avg = (heightMap[(i - centerPoint + heightReso) % heightReso, j] +
                            heightMap[(i + centerPoint) % heightReso, j] +
                            heightMap[i, (j + centerPoint) % heightReso] +
                            heightMap[i, (j - centerPoint + heightReso) % heightReso]) / 4;

                    float offset = Random.Range(0.0f, 1.0f) * 2 * range - range;
                    heightMap[i, j] = avg + offset;

                    //set the points thats on the edge
                    if (i == 0)
                        heightMap[heightReso - 1, j] = avg + offset;
                    if (j == 0)
                        heightMap[i, heightReso - 1] = avg + offset;
                }
            }
        }
        //scale the height map to values between 0 and 0.5 so it renders correctly
        float scale = (float)seed * 2;
        for (int i = 0; i < heightReso; i++)
        {
            for (int j = 0; j < heightReso; j++)
            {
                heightMap[i, j] /= scale;

            }
        }
        return heightMap;
    }

    //according to the dimension(of the map),generate all the vertices for that polygon
    public ArrayList ConstructAllGrids(int dimension)
    {
        ArrayList grids = new ArrayList();
        for (int i = 0; i < dimension; i++)
        {
            for (int j = 0; j < dimension; j++)
            {

                grids.AddRange(ConstructOneGrid(new Vector2(i, j)));//add one more square to the list
            }
        }

        return grids;
    }
    //generate one square according to its pos on the terrain
    public ArrayList ConstructOneGrid(Vector2 coor)
    {
        ArrayList grid = new ArrayList();

        //bottom triangle
        grid.Add(new Vector3(coor.x, heightMap[(int)coor.x+1, (int)coor.y+1] * 50, coor.y));
        grid.Add(new Vector3(coor.x + 1, heightMap[(int)coor.x + 2, (int)coor.y+1] * 50, coor.y));
        grid.Add(new Vector3(coor.x + 1, heightMap[(int)coor.x + 2, (int)coor.y + 2] * 50, coor.y + 1));
        //top triangle
        grid.Add(new Vector3(coor.x, heightMap[(int)coor.x+1, (int)coor.y+1] * 50, coor.y));
        grid.Add(new Vector3(coor.x + 1, heightMap[(int)coor.x + 2, (int)coor.y + 2] * 50, coor.y + 1));
        grid.Add(new Vector3(coor.x, heightMap[(int)coor.x+1, (int)coor.y + 2] * 50, coor.y + 1));
        
        //x,z value will determine where the square is at,y value is from the height map
        return grid;

    }
    //find the max height for the terrain
    public float FindMax(Vector3[] list)
    {
        float max = 0;
        for (int i = 0; i < list.Length; i++)
        {
            if (list[i].y > max)
            {
                max = list[i].y;
            }
        }
        return max;
    }
    //make a hole for the water section
    public void MakeHole()
    {
        for (int i = (int)(heightReso*0.4); i < heightReso * 0.7; i++)
        {
            for (int j = (int)(heightReso * 0.4); j < (int)(heightReso * 0.7); j++)
            {
                heightMap[i, j] *= 0.5f;
            }
        }
    }
    //generate the color list according to the height
    public Color[] GenerateColors(Vector3[] vertices)
    {
        Color[] colors = new Color[vertices.Length];
        float maxHeight = FindMax(vertices);

        for (int i = 0; i < vertices.Length; i++)
        {
            if (vertices[i].y > maxHeight * 0.9)
            {
                colors[i] = Color.cyan;
            }
            else if (vertices[i].y > maxHeight * 0.7 && vertices[i].y <= maxHeight * 0.9)
            {
                colors[i] = Color.white;
            }
            else if (vertices[i].y <= 0.7 * maxHeight && vertices[i].y > 0.5 * maxHeight)
            {
                colors[i] = Color.green;
            }
            else
            {
                colors[i] = Color.yellow;
            }

        }
        return colors;
    }




}